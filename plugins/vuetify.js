import Vue from 'vue' //68.1K (gzipped: 34.3K)
import Vuetify from 'vuetify/lib'


export default new Vuetify({
    icons: {
        iconfont: 'mdi'
    },
    theme: {
        dark: true
    }
})