import axios from "axios"

export const state = () => ({
    user: null
  })
// export const state = {
    
// }


export const mutations = {
    SET_USER_DATA(state, userData){
        state.user = userData
        localStorage.setItem('user', JSON.stringify(userData))
        axios.defaults.headers.common['Authorization'] = `Bearer ${userData.token_response}`
    },
    CLEAR_USER_DATA(){
        localStorage.removeItem('user')
        location.reload()

    }
}


export const actions = {
    register({commit}, credentials){
        return axios.post('//127.0.0.1:8000/api/auth/register/', credentials).then(
            ({data}) => {
                console.log("User data is: ", data)
                commit('SET_USER_DATA', data)
            }
        ).catch(err => console.log(err))
    },
    login({commit}, credentials){
        return axios.post('//127.0.0.1:8000/api/auth/login', credentials).then(
            ({data}) => {
                console.log("User data is: ", data)
                console.log(this.$auth.state.loggedIn)
                console.log(this.$auth.token_response)
                this.$auth.state.loggedIn = true
                commit('SET_USER_DATA', data)
            }
        ).catch(err => console.log(err))
    },
    logout({commit}){
        commit('CLEAR_USER_DATA')
    }
    
}

export const getters = {
    loggedIn (state){
        return !!state.user
    }
}
